<?php


$api = app('Dingo\Api\Routing\Router');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



$api->version('v1', ['prefix' => '/api', 'namespace' => 'App\Api\Controllers'], function ($api) {

    # 获取用户openid
    $api->get('user/getOpenid','UserController@getOpenid');
    # 添加用户
    $api->post('user','UserController@handleUser');
    # 添加用户
    $api->get('user/info','UserController@getUserInfo');

    # 搜索
    $api->post('search','SearchController@search');

    # 获取优惠券信息
    $api->get('coupon/info','CouponController@info');
    # 获取优惠券评论
    $api->get('coupon/comments','CouponController@comments');
    # 对优惠券进行评论
    $api->post('coupon/commenting','CouponController@commenting');
    # 获取我的优惠券
    $api->get('coupon/mycoupon','CouponController@mycoupon');
    # 获取我的优惠券三种状态的数量
    $api->get('coupon/mycoupon_allnum','CouponController@mycoupon_allnum');
    # 获取附近优惠券
    $api->get('coupon/lists','CouponController@lists');
    # 发布优惠券
    $api->post('coupon/publish','CouponController@publish');
    # 获取我发布的优惠券
    $api->get('coupon/alycoupons','CouponController@alycoupons');
    # 上架或下架我发布的优惠券
    $api->get('coupon/upOrdown','CouponController@upOrdown');
    # 优惠券评论点赞
    $api->post('coupon/commentslike','CouponController@commentsLike');

    #核销员列表
    $api->get('clerk/list','ClerkController@list');
    #删除核销员
    $api->get('clerk/delete','ClerkController@delete');
    #添加核销员二维码
    $api->get('clerk/code','ClerkController@code');
    #绑定核销员
    $api->get('clerk/bind','ClerkController@bind');
    #插入浏览数量
    $api->get('coupon/browse','CouponController@browse');

    #订单二维码
    $api->get('order/code','OrderController@code');
    #订单核销
    $api->get('order/over','OrderController@over');
    #确认核销
    $api->get('order/confirm','OrderController@confirm');
    # 下单
    $api->post('order/createOrder','OrderController@createOrder');
    # 支付回调地址
    $api->post('order/orderSucc','OrderController@orderSucc');


    # 首页Banner
    $api->get('banner','BannerController@index');
    # 数据统计我的发布的优惠券
    $api->post('mycoupon','StatisticsController@myCoupon');
    //数据统计我的发布的优惠券的浏览使用未使用数量
    $api->post('mycouponnum','StatisticsController@myCouponNum');
    # 我的优惠券记录
    $api->post('record','RecordController@record');
    # 获取优惠券评论
    $api->get('coupon/comments','CouponController@comments');


});


