<?php

/**
 * 后台路由
 */

# 首页
Route::get('/manage/user/login', 'Manage\UsersController@loginPage');
Route::post('/manage/user/login', 'Manage\UsersController@login');

Route::group(['prefix' => '/manage', 'namespace' => 'Manage', 'middleware' => 'login'], function ($app) {
    # 首页
    $app->get('/', 'Home\IndexController@index');
    # Menu
    $app->resource('menu', 'Home\MenuController');
    #评论管理
    $app->resource('comment', 'CommentController');
    $app->get('comment/show/{id}','CommentController@show');
    $app->get('comment/destroy/{id}','CommentController@destroy');
    $app->patch('comment/audit/{id}', 'CommentController@audit');
    #banaer
    $app->resource('banner', 'BannerController');
     #用户
    $app->resource('users','UsersController');
    #优惠券
    $app->resource('coupon','CouponController');
    $app->patch('coupon/hide/{id}','CouponController@hide');
    $app->patch('coupon/audit/{id}','CouponController@audit');
    #订单
    $app->resource('order','OrderController');



});


