<?php

namespace App\Api\Controllers;
use App\Api\TransFormers\BannerTransformer;
use App\Models\Banner;

class BannerController extends BaseController
{

    /**
     * 首页Banner
    */
    public function index()
    {
        $list = Banner::get();
        return $this->response->collection($list,new BannerTransformer());
    }

}