<?php
namespace App\Api\Controllers;
use App\Api\Requests\StatisticsRequest;
use App\Api\Requests\RecordRequest;
use App\Api\TransFormers\StatisticsTransformer;
use App\Models\User;
use App\Models\Order;
use App\Models\Coupon;
use App\Models\Browse;
/*
	*
	*数据统计

*/
class StatisticsController extends BaseController
{
	//获取我的优惠券
	 public function myCoupon(StatisticsRequest $request){
	 	$userInfo = User::where('openid', $request->get('id'))->first();
	 	//查询该用户下的所有优惠券
	 	$coupon_list=Coupon::where(['user_id'=>$userInfo->id])->get();
	 return $this->response->collection($coupon_list, new StatisticsTransformer());

	}

//获取对应优惠券的的浏览，使用未使用情况
	public function myCouponNum(RecordRequest $request){
		$id=$request->get('id');
		$coupn = Coupon::where(['id'=>$id])->first();
		if($coupn){
			//未使用数量
			$no_num=Order::where(['coupon_id'=>$coupn->id,'status'=>2])->count();
			$user_num=Order::where(['coupon_id'=>$coupn->id,'type'=>2])->count();
			$browse_num=Browse::where(['coupon_id'=>$coupn->id])->count();
			$num['num']=[
			'no_num'=>$no_num,//lin
			'user_num'=>$user_num,
			'browse_num'=>$browse_num,
			];
			return $num;
		}else{
 			 return $this->response->error('没有对应ID', 404);
 		}

	}


	

	

}