<?php
namespace App\Api\Controllers;
use App\Api\Requests\RecordRequest;
use App\Api\TransFormers\RecordTransformer;
use App\Api\TransFormers\BrowseTransformer;
use App\Models\User;
use App\Models\Order;
use App\Models\Coupon;
use App\Models\Browse;
/*
*数据统计
*/
class RecordController extends BaseController
{
	//获取我的优惠券
	 public function record(RecordRequest $request){
 		$type=$request->get('type');
 		$id=$request->get('id');
 		$coupn = Coupon::where(['id'=>$id])->first();
 		//1浏览 2领取 3使用
 		if($coupn){
 			if($type==1){
 				$pageSize = 10;
 				$order_list=Browse::with('user')->where(['coupon_id'=>$coupn->id])
 				->latest('browsed_at')
 				->paginate($pageSize); 			
		        return $this->response->paginator($order_list, new BrowseTransformer());
 			}else if($type==2){
		       $pageSize = 10;
 				$order_list=Order::with('user')->where(['coupon_id'=>$coupn->id,'status'=>2])
 				->latest('created_at')
 				->paginate($pageSize);
		        return $this->response->paginator($order_list, new RecordTransformer());
 			}else if($type==3){
		        $pageSize = 10;
 				$order_list=Order::with('user')->where(['coupon_id'=>$coupn->id,'status'=>2,'type'=>2])
 				->latest('created_at')
 				->paginate($pageSize);
		        return $this->response->paginator($order_list, new RecordTransformer());	
 			}
 		}else{
 			 return $this->response->error('没有对应ID', 404);
 		}
	}
	
}