<?php
namespace App\Api\TransFormers;
use App\Models\Order;
use League\Fractal\TransformerAbstract;
//统计数据中的我的优惠券
class RecordTransformer extends TransformerAbstract

{
    public function transform(Order $item)
    {
        $list = $item->toArray();
        $time='';
        if($item->type==1){
				$time="".$item->paid_at."";
        }elseif($item->type==2){
        	    $time="".$item->used_at."";
        }
        return [
            'avatar_url' => $item->user->avatar,
            'name' => $item->user->name,
            'time'=>$time

            
        ];
    }
}


?>


