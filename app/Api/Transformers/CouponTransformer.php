<?php

namespace App\Api\TransFormers;

use App\Models\Coupon;
use League\Fractal\TransformerAbstract;

class CouponTransformer extends TransformerAbstract
{
    public function transform(Coupon $item)
    {
        $list = [
            'id' => $item->id,
            'name' => $item->name,
            'browse' => $item->browse,
            'distance' => $item->distance,
            'price' => $item->price,
            'longitude' => $item->longitude,
            'latitude' => $item->latitude,
            'img' => $item->img,
            //'isbuy'=>$item->isBuy($item->id , request()->get('openid'))
        ];
        return $list;
    }
}