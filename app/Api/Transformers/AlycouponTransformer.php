<?php

namespace App\Api\TransFormers;

use App\Models\Coupon;
use League\Fractal\TransformerAbstract;

class AlycouponTransformer extends TransformerAbstract
{
    public function transform(Coupon $item)
    {
        $list = [
            'id' => $item->id,
            'name' => $item->name,
            'start'=>$item->dateTotext($item->start),
            'end'=>$item->dateTotext($item->end),
            'status'=>$item->status,
            'img' => $item->img
        ];
        if($item->verify == 1){
            $list['status'] = 3; // 未审核
        }
        if($item->verify == 3){
            $list['status'] = 4; // 审核未通过
        }
        if($status = $item->isOverTime($item->end)){
            $list['status'] = $status; // 2 已过期
        }
        return $list;
    }
}