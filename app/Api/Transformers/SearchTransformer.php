<?php

namespace App\Api\TransFormers;

use App\Models\Coupon;
use League\Fractal\TransformerAbstract;

class SearchTransformer extends TransformerAbstract
{
    public function transform(Coupon $item)
    {
        return $item->toArray();
    }
}