<?php

namespace App\Api\TransFormers;

use App\Api\Requests\CouponRequest;
use App\Models\Comment;
use App\Models\User;
use League\Fractal\TransformerAbstract;

class CommentsTransformer extends TransformerAbstract
{

    public function transform(Comment $item)
    {
        $openid = app(CouponRequest::class)->get('openid');
        $user_id = app(User::class)->where(['openid'=>$openid])->first()->id;
        $info = [
            'id' => $item->id,
            'content' => $item->content,
            'num'=>$item->fabulous_num,
            'time' => (string)$item->created_at,
            'avatar'=>$item->user->avatar,
            'name'=>$item->user->name,
            'islike'=>$item->islike($item->id,$user_id)
        ];

        return $info;
    }
}