<?php
namespace App\Api\TransFormers;
use App\Models\Coupon;
use League\Fractal\TransformerAbstract;
//统计数据中的我的优惠券
class StatisticsTransformer extends TransformerAbstract

{
    public function transform(Coupon $item)
    {
        $list = $item->toArray();
        return [
            'id' => $item->id,
            'name' => $item->name,
        ];
    }
}


?>