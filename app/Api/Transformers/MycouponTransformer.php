<?php

namespace App\Api\TransFormers;

use App\Models\Order;
use League\Fractal\TransformerAbstract;

class MycouponTransformer extends TransformerAbstract
{
    public function transform(Order $item)
    {
        $list = [
            'coupon_id' => $item->coupon_id,
            'start'=>$item->coupon->dateTotext($item->coupon->start),
            'end'=>$item->coupon->dateTotext($item->coupon->end),
            'name' => $item->coupon->name,
            'img' => $item->coupon->img
        ];
        //$list = $item->toArray();
        return $list;
    }
}