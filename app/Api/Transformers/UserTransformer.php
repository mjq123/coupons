<?php

namespace App\Api\TransFormers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $item)
    {
        return [
            'id' => $item->id,
            'gender' => $item->gender,
            'avatar' => $item->avatar,
            'name' => $item->name,
            'city' => $item->city,
            'is_manger' => (bool)$item->coupons->count()
        ];
    }
}