<?php

namespace App\Api\TransFormers;

use App\Api\Requests\CouponRequest;
use App\Models\Coupon;
use League\Fractal\TransformerAbstract;

class CouponinfoTransformer extends TransformerAbstract
{

    public function transform(Coupon $item)
    {
        $lng = app(CouponRequest::class)->get('lng');
        $lat = app(CouponRequest::class)->get('lat');
        $info = [
            'id' => $item->id,
            'name' => $item->name,
            'limit' => $item->limit,
            'start' => $item->dateChange($item->start),
            'end' => $item->dateChange($item->end),
            'week' => $item->weekText[$item->week],
            'time' => $item->timeText[$item->time],
            'address' => $item->address,
            'phone' => $item->phone,
            'price'=>$item->price,
            'lng'=>$item->longitude,
            'lat'=>$item->latitude,
            'distance'=>$item->distance($lng,$lat,$item),
            'lingnum'=>$item->lingnum($item->id)
        ];

        return $info;
    }
}