<?php
namespace App\Api\TransFormers;
use App\Models\Browse;
use League\Fractal\TransformerAbstract;
//统计数据中的我的优惠券
class BrowseTransformer extends TransformerAbstract

{
    public function transform(Browse $item)
    {
        $list = $item->toArray();

        return [
            'avatar_url' => $item->user->avatar,
            'time'=>"".$item->browsed_at."",
            'name' => $item->user->name,

            
        ];
    }
}


?>