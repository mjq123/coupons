<?php

namespace App\Api\TransFormers;

use League\Fractal\TransformerAbstract;

class ClerkTransformer extends TransformerAbstract
{

    public function transform($item)
    {
        $info = [
            'id' => $item->id,
            'name' => $item->user->name,
            'avator' => $item->user->avatar,
            'created_at' => $item->created_at.='',
        ];

        return $info;
    }
}