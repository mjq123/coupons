<?php

namespace App\Api\TransFormers;

use App\Models\Banner;
use League\Fractal\TransformerAbstract;

class BannerTransformer extends TransformerAbstract
{

    public function transform(Banner $item)
    {
        $info = [
            'name'=>$item->name,
            'imgurl'=>$item->imgs,
            'coupon_id'=>$item->coupon_id,
        ];
        return $info;
    }
}