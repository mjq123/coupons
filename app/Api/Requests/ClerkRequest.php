<?php

namespace App\Api\Requests;


use Dingo\Api\Http\FormRequest;

class ClerkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $commons = [

        ];
        return get_request_rules($this, $commons);
    }

    // 核销员列表
    public function listRules()
    {
        return [

        ];
    }
    /**
     * 错误信息
     */
    public function messages()
    {
        return [

        ];
    }
}
