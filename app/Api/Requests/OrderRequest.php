<?php

namespace App\Api\Requests;


use Dingo\Api\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $commons = [

        ];
        return get_request_rules($this, $commons);
    }

    public function createOrderRules()
    {
        return [
            'openid'=>'required|exists:users,openid',
            'coupon_id'=>'required|exists:coupons,id'
        ];
    }

    public function message()
    {
        return [
            'openid.*'=>'获取用户信息失败',
            'coupon_id.*'=>'获取优惠券信息失败'
        ];
    }

}
