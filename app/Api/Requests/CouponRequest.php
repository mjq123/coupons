<?php

namespace App\Api\Requests;


use Dingo\Api\Http\FormRequest;

class CouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $commons = [

        ];
        return get_request_rules($this, $commons);
    }

    // 优惠券详情
    public function infoRules()
    {
        return [
            'id' => 'required|exists:coupons,id',
            'lng'=>'required',
            'lat'=>'required'
        ];
    }

    // 获取某个优惠券的评论
    public function commentsRules()
    {
        return [
            'id' => 'required|exists:coupons,id'
        ];
    }

    // 对某个优惠券进行评论
    public function commentingRules()
    {
        return [
            'id' => 'required|exists:coupons,id',
            'openid' => 'required|exists:users,openid',
            'comments' => 'required'
        ];
    }
    
    // 对优惠券评论点赞
    public function commentsLikeRules()
    {
        return [
            'openid'=>'required|exists:users,openid',
            'comment_id'=>'required|exists:comments,id'
        ];
    }

    // 获取附近优惠券
    public function listsRules()
    {
        return [
            'lng' => 'required',
            'lat' => 'required',
        ];
    }

    // 我的优惠券
    public function mycouponRules()
    {
        return [
            'openid' => 'required|exists:users,openid',
            'status' => 'required' // 使用状态
        ];
    }

    // 我的优惠券三种状态的个数
    public function mycoupon_allnumRules()
    {
        return [
            'openid'=>'required|exists:users,openid'
        ];
    }

    // 获取我发布的优惠券
    public function alycouponsRules()
    {
        return [
            'openid'=>'required|exists:users,openid'
        ];
    }

    // 我的优惠券上下架
    public function upOrdownRules()
    {
        return [
            'openid'=>'required|exists:users,openid',
            'id'=>'required|exists:coupons,id'
        ];
    }

    public function publishRules()
    {
        return [
            'openid' => 'required|exists:users,openid',
            'name' => 'required',
            'type' => 'required',
            'prefer_type' => 'required',
            'prefer_value' => 'required',
            'num' => 'required',
            'phone' => 'required',
            'week' => 'required',
            'time' => 'required',
            'start' => 'required',
            'end' => 'required',
            'lng' => 'required',
            'lat' => 'required',
            'address' => 'required',
            'img' => 'required'
        ];
    }

    /**
     * 错误信息
     */
    public function messages()
    {
        return [
            'status.required' => '优惠券状态不能为空',
            'id.*' => '优惠券信息不存在',
            'openid.*' => '获取用户信息失败',
            'name.required' => '请输入名称',
            'type.required' => '请选择行业类型',
            'prefer_type.required' => '请选择优惠类型',
            'prefer_value.required' => '请填写优惠值',
            'num.required' => '请填写数量',
            'phone.required' => '请填写联系电话',
            'week.required' => '请填写周几可用',
            'time.required' => '请填写可用时间段',
            'start.required' => '请选择开始日期',
            'end.required' => '请选择过期日期',
            'lng.required' => '请填写坐标位置',
            'lat.required' => '请填写坐标位置',
            'address.required' => '请填写地址',
            'img.*' => '请上传图片',
            'comments.required' => '请填写评论内容',
            'comment_id.*'=>'获取评论信息失败'
        ];
    }
}
