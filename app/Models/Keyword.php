<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Api\TransFormers\CoalGassTransformer;
use App\Api\TransFormers\DeliverWaterTransformer;
use App\Api\TransFormers\HouseMovingTransformer;
use App\Api\TransFormers\PublicToiletTransformer;
use App\Api\TransFormers\RecoveryTransformer;
use App\Api\TransFormers\RentalTransformer;
use App\Api\TransFormers\UnlockTransformer;

class Keyword extends Model
{
    public $fillable = [
        'content' , 'common_id' , 'common_type' ,'created_at' ,'updated_at'
    ];
}
