<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CommentsLike extends Model
{
    use softDeletes;

    // 获取用户
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /**
     * 获取用户
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coupon()
    {
        return $this->belongsTo(Coupon::class,'coupon_id');
    }

}
