<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Banner extends Model

{
    
	use softDeletes;

    public function getImgsAttribute($img)
    {
        if (!filter_var($img, FILTER_VALIDATE_URL))
            return env('OSS_ENDPOINT') . env('OSS_PREFIX') . $img;
        return $img;
    }
    // 获取优惠券
    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }

}
