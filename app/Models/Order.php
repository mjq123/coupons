<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Order extends Model

{
    use softDeletes;
   
    public $statusText = [
        1 => '未支付',
        2 => '已支付',
        3 => '已完成',
        4=> '已取消',
        5 => '支付失败',
    ];
    /**
     * 获取用户 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /**
     * 获取用户优惠券 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coupon()
    {
        return $this->belongsTo(Coupon::class,'coupon_id');
    }




}
