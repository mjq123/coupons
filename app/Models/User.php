<?php

namespace App\Models;

use App\Http\Controllers\Manage\Management\CoalGassesController;
use App\Http\Controllers\Manage\Management\DeliverWaterController;
use App\Http\Controllers\Manage\Management\PublicToiletsController;
use App\Http\Controllers\Manage\Management\RecoveriesController;
use App\Http\Controllers\Manage\Management\UnlockController;
use App\Http\Controllers\Manage\Service\HouseMoveController;
use App\Http\Controllers\Manage\Service\RentalController;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public $genderText = [
        1 => '男',
        2 => '女',
        3 => '未知',
    ];
    public $typeText = [
        1 => '微信',
        2 => '管理员',
    ];


    /**
     * 用户详细信息
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function info()
    {
        return $this->hasOne(UserInfo::class);
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = encrypt($password);
    }
    public function clerk()
    {
        return $this->hasMany(WriteOffs::class);
    }

    /**
     * 用户的优惠券
     */
    public function coupons()
    {
        return $this->hasMany(Coupon::class);
    }

}
