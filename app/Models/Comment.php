<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    //
    use softDeletes;
    public $statusText = [
        1 => '待审核',
        2 => '已审核',
        3 => '垃圾信息',
    ];

    // 获取用户
    public function user()
    {
        return $this->belongsTo(User::class);
    }
 
    public function coupon()
    {
        return $this->belongsTo(Coupon::class,'coupon_id');
    }

    # 用户是否点赞改评论
    public function islike($comment_id,$user_id)
    {
        if(!empty($comment_id) && !empty($user_id)){
            $islike = app(CommentsLike::class)->where([
                ['comment_id',$comment_id],
                ['user_id',$user_id]
            ])->first();
            if(!empty($islike->id)){
                return 1;
            }
        }
        return 0;
    }
}
