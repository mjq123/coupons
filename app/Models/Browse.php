<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Browse extends Model
{
    //
    use softDeletes;
    public $table="browse";

    // 获取用户
    public function user()
    {
        return $this->belongsTo(User::class);
    }
 
    public function coupon()
    {
        return $this->belongsTo(Coupon::class,'coupon_id');
    }

}
