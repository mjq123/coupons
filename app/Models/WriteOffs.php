<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Api\TransFormers\CoalGassTransformer;
use App\Api\TransFormers\DeliverWaterTransformer;
use App\Api\TransFormers\HouseMovingTransformer;
use App\Api\TransFormers\PublicToiletTransformer;
use App\Api\TransFormers\RecoveryTransformer;
use App\Api\TransFormers\RentalTransformer;
use App\Api\TransFormers\UnlockTransformer;

class WriteOffs extends Model

{
    public $table="write_offs";
    public $fillable = [
        'user_id' , 'write_offs_id' , 'common_type'
    ];


    /**
     * 获取用户 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class,'write_offs_id');
    }



}
