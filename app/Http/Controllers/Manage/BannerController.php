<?php

namespace App\Http\Controllers\Manage;
use App\Http\Controllers\Common\UploadController;
use App\Http\Requests\Manage\BannersRequest;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\Coupon;





class BannerController extends BaseController
{
    use UploadController;
    //用户列表页
    public function index(Request $request)
    {
        $ins = new Banner();
        $where = [];
        if ($tmpInput = $request->get('name')) {
            array_push($where, ['name', 'like', '%' . $tmpInput . '%']);
        }
        $lists=$ins->latest('id')->where($where)->paginate(10);
    	return view('manage.banner.index',['lists'=>$lists]);
    }
    /**
     * @param 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(){
        //获取优惠券
        $coupon=new Coupon();
        $lists=$coupon->get();
        return view('manage.banner.add',['lists'=>$lists]);
    }
    /**
     * @param 
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(BannersRequest $request){
        //获取图片
        $banner= new Banner;

        $banner->name=$request->get('name');
        $banner->coupon_id=$request->get('coupon_id');

        $imgs=  $request->file('imgs');
        if(!empty($imgs)){
             $fileName =  time() . $imgs->getClientOriginalName();
             $res=$this->uploadFile($fileName, $imgs);
             $banner->imgs = $fileName;
        }
        if($banner->save()){     
            return redirect('/manage/banner');
        }else{
            return redirect()->back()->withInput()->withErrors('添加失败');
        }

  

    }


   /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit    ($id){
        //获取优惠券
        $coupon=new Coupon();
        $lists=$coupon->get();

        $info = Banner::findOrFail($id);
        return view('manage.banner.edit',compact('info','lists'));
    }
    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */

    public function update(BannersRequest $request,$id){
        //获取图片
        $banner= new Banner;
        $info = Banner::findOrFail($id);
        $imgs=$request->file('imgs');
        $banner = $banner->find($id);
        $banner->name=$request->get('name');
        $banner->coupon_id=$request->get('coupon_id');
        if(!empty($imgs)){
             $fileName =  time() . $imgs->getClientOriginalName();
             $res=$this->uploadFile($fileName, $imgs);
             $banner->imgs = $fileName;
        }else{
             $banner->imgs = $info['imgs'];
        }
        if($banner->save()){     
            return redirect('/manage/banner');
        }else{
            return redirect()->back()->withInput()->withErrors('修改失败');
        }

    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $info = Banner::findOrFail($id);
        $info->delete();
        session()->flash('success','删除成功');
        return redirect()->back();
    }




}
