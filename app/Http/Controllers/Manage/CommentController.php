<?php

namespace App\Http\Controllers\Manage;
use App\Http\Requests\Manage\UserRequest;
use Illuminate\Http\Request;
use App\Http\Requests\Manage\CommentRequest;
use App\Models\User;
use App\Models\WriteOffs;
use App\Models\Comment;
use App\Models\AuditLog;
use Carbon\Carbon;


class CommentController extends BaseController
{
    //用户列表页
    public function index(Request $request)
    {
        $ins = new Comment();
        $where = [];
        if ($tmpInput = $request->get('content')) {
            array_push($where, ['content', 'like', '%' . $tmpInput . '%']);
        }
        $user_name = $request->get('name');
        if ($user_name) {
            $ins = $ins->whereHas('user' ,function ($query) use ($user_name) {
                $query->where('name', 'like', '%' . $user_name . '%');
            });
        }
        if ($tmpInput = $request->get('status')) {
            array_push($where, ['status', 'like', '%' . $tmpInput . '%']);
        }
        $lists=$ins->latest('id')->with('coupon')->with('user')->where($where)->paginate(10);
      
        //dd($lists->toArray());die;
    	return view('manage.comment.index',['lists'=>$lists]);
    }

    /**
     * 显示
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $info = Comment::with('user')->findOrFail($id);
        return view('manage.comment.show', compact('info'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $info = Comment::findOrFail($id);
        $info->delete();
        session()->flash('success','删除成功');
        return redirect()->back();
    }


    /**
     * 审核
     * @param HouseMoveRequest $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function audit(CommentRequest $request, $id)
    {

        $info = Comment::findOrFail($id);
        # 修改 status
        $info->status = $request->get('type');
        try {
            $info->save();
            # 添加记录
            AuditLog::insert([
                'remark' => $request->get('remark'),
                'value' => $request->get('type'),
                'common_id' => $info->id,
                'common_type' => Comment::class,
                'created_at' => Carbon::now()
            ]);
        } catch (\Exception $error) {
            return $this->rollback(null, back()->withInput());
        }

        \DB::commit();
         return $this->success('审核成功', redirect($request->get('url')));
    }




}
