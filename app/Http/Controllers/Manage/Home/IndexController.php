<?php

namespace App\Http\Controllers\Manage\Home;

use App\Http\Controllers\Manage\BaseController;

class IndexController extends BaseController
{
    public function index()
    {
        $txt = '附近优惠券 ';

        return view('manage/home/index')->with(compact('txt'));
    }
}
