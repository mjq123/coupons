<?php

namespace App\Http\Controllers\Manage;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Models\Coupon;
use Carbon\Carbon;
use App\Http\Controllers\Common\UploadController;

class CouponController extends BaseController
{
    use UploadController;
    /**
     * Display a listing of the resource.
     *优惠券管理列表页
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $where = [];
        if($tmpInput = $request->get('name'));
        array_push($where, ['name', 'like', '%' . $tmpInput . '%']);
        if ($tmpInput = $request->get('key'))
            array_push($where, ['content', 'like', '%' . $tmpInput . '%']);
        if ($tmpInput = $request->get('type'))
            array_push($where, ['type',$tmpInput]);
        $lists = Coupon::where($where)->orderBy('id','desc')->paginate(10);
        return view('manage.coupon.index',['lists'=>$lists]);
    }

    public function hide($id)
    {
        $info = Coupon::findOrFail($id);
        $info->status = (bool)(!$info->status);
        $info->save();
        return redirect()->back();
    }

    public function show($id)
    {
        //
        $info = Coupon::findOrFail($id);
        $info->content = str_replace('，',',',$info->content);
        $info->save();
        return view('manage.coupon.show', compact('info'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $info = Coupon::findOrFail($id);
        $info->start = date('Y-m-d',strtotime($info->start));
        $info->end = date('Y-m-d',strtotime($info->end));
        return view('manage.coupon.edit', compact('info'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $info = $request->except('_token','_method','url');
        $coupon = Coupon::find($id);
        $img = $request->file('img');
        if(!empty($img)){
            $fileName =  time() . $img->getClientOriginalName();
            $this->uploadFile($fileName, $img);
            $info['img'] = $fileName;
        }else{
            $info['img'] = $coupon->img;
        }
        $res = Coupon::where("id",$id)->update($info);
        if($res){
            session()->flash('success','更新成功');
            return redirect(url('manage/coupon'));
        }else{
            session()->flash('info','更新失败，请联系管理员');
            return redirect(url('manage/coupon'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $info = Coupon::findOrFail($id);
        Order::where('coupon_id',$id)->delete();
        $info->delete();
        session()->flash('success','删除成功');
        return back();
    }
    /**
     * 审核
     */
    public function audit(Request $request,$id)
    {
        $info = Coupon::findOrFail($id);
        $verify = $request->get('verify');
        $info->verify = $verify;
        $info->verified_at = Carbon::now();
        try {
            $info->save();
            # 添加记录
            \App\Models\AuditLog::insert([
                'remark' => $request->get('remark'),
                'value' => $verify,
                'common_id' => $info->id,
                'common_type' => Coupon::class,
                'created_at' => Carbon::now()
            ]);
        } catch (\Exception $error) {
            return $this->rollback(null, back()->withInput());
        }

        \DB::commit();
        return $this->success('审核成功', redirect($request->get('url')));
    }
}
