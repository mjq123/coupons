<?php

namespace App\Http\Controllers\Manage;

use Illuminate\Http\Request;
use App\Models\Order;

class OrderController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $where = [];
        $w = [];
        if($tmpInput = $request->get('name'));
        array_push($w, ['name', 'like', '%' . $tmpInput . '%']);
        if ($tmpInput = $request->get('no'))
            array_push($where, ['order_no', 'like', '%' . $tmpInput . '%']);
        if ($tmpInput = $request->get('status'))
            array_push($where, ['status', 'like', '%' . $tmpInput . '%']);
        $lists = Order::whereHas('user', function ($query)use($w) {
            $query->where($w);
        })->where($where)->orderBy('id','desc')->paginate(10);
        return view('manage.order.index',['lists'=>$lists]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $info = Order::findOrFail($id);
        return view('manage.order.show', compact('info'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $info = Order::findOrFail($id);
        $info->delete();
        session()->flash('success','删除成功');
        return back();
    }
}
