<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimeToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('orders', function (Blueprint $table) {
            $table->timestamp('used_at')->comment('使用时间')->nullable();

        });
        Schema::table('browse', function (Blueprint $table) {
            $table->timestamp('browsed_at')->comment('浏览时间')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('used_at');
        });
        Schema::table('browse', function (Blueprint $table) {
            $table->dropColumn('browsed_at');
        });
    }
}
