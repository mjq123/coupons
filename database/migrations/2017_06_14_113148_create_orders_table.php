<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *订单表
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->comment('用户id（用户标识）');
            $table->unsignedInteger('coupon_id')->comment('优惠券id（优惠券id）');
            $table->unsignedTinyInteger('type')->default(1)->comment('默认1未使用,2：已使用');
            $table->unsignedTinyInteger('status')->default(1)->comment('默认1未支付,2：已支付，3已完成,4已取消 5 支付失败');
            $table->decimal('money',8,2)->comment('订单金额');
            $table->unsignedInteger('order_no')->comment('订单号');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */


    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
