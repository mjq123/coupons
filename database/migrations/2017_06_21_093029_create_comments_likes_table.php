<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsLikesTable extends Migration
{
    public function up()
    {
        Schema::create('comments_likes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('comment_id')->comment('评论表id');
            $table->unsignedInteger('user_id')->comment('用户表id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments_likes');
    }
}
