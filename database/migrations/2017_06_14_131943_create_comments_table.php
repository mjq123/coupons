<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *评论表
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->comment('用户id（用户标识）');
            $table->unsignedInteger('coupon_id')->comment('优惠券id（优惠券id）');
            $table->unsignedTinyInteger('status')->default(1)->comment('默认1待审核,2：已审核，3：垃圾信息');
            $table->string('content')->comment('评论内容')->nullable();
            $table->Integer('fabulous_num')->comment('点赞数量');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
