<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *优惠券
     * @return void
     */
    public function up()
    {
        //
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->comment('用户关联id');
            $table->string('name')->comment('门店名称');
            $table->tinyInteger('type')->comment('2美食,3娱乐,4酒店,5服务,6教育,7丽人,8其他')->nullable();
            $table->string('limit')->comment('使用限制')->nullable();
            $table->string('img')->comment('图片');
            $table->decimal('price',5,2)->comment('优惠券售价')->default(0.00);
            $table->tinyInteger('prefer_type')->comment('优惠类型,1元2折');
            $table->decimal('prefer_value',5,2)->comment('优惠值');
            $table->unsignedInteger('count')->comment('优惠劵数量');
            $table->unsignedInteger('use_count')->comment('优惠劵使用数量')->default(0);
            $table->string('phone')->comment('电话');
            $table->string('address')->comment('门店地址');
            $table->tinyInteger('week')->comment('周几可用，1-7》周一-周日、8工作日、9周末、10每天');
            $table->tinyInteger('time')->comment('1上午,2下午,3晚上,4全天');
            $table->date('start')->comment('开始时间');
            $table->date('end')->comment('结束时间');
            $table->unsignedInteger('browse')->comment('浏览次数')->default(0);
            $table->boolean('status')->comment('false隐藏true显示')->default(false);
            $table->tinyInteger('verify')->comment('1待审核2已审核3驳回4垃圾信息')->default(1);
            $table->timestamp('verified_at')->comment('审核时间')->nullable();
            $table->double('longitude',10,6)->comment('经度');
            $table->double('latitude',10,6)->comment('纬度');
            $table->unsignedTinyInteger('times')->comment('最大领取次数')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('coupons');
    }
}
