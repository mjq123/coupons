<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *用户表
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->char('openid', 28)->nullable()->comment('用户openid');
            $table->string('password')->nullable()->comment('密码');
            $table->tinyInteger('gender')->default(3)->comment('用户性别 1：男、2：女 3：未知');
            $table->string('avatar')->nullable()->comment('用户头像');
            $table->string('name');
            $table->string('city')->nullable()->comment('城市');
            $table->tinyInteger('type')->default(1)->comment('用户类型 1 微信用户 2 后台用户');
            $table->softDeletes();
            $table->timestamps();
            $table->index('openid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
