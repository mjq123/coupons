<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFieldFromCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::dropIfExists('keywords');
        Schema::table('coupons', function (Blueprint $table) {
            $table->string('content')->comment('关键字内容,中间用,隔开');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('coupons', function (Blueprint $table) {
            $table->dropColumn('content');

        });
    }
}
