<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaylogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paylogs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->default(0)->comment('订单id');
            $table->decimal('money',8,2)->comment('订单金额');
            $table->string('prepay_id')->nullable()->comment('会话id');
            $table->string('order_no',30)->comment('订单号');
            $table->tinyInteger('status')->default(0)->comment('0:未支付，1:支付成功,，2:支付失败');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paylogs');
    }
}
