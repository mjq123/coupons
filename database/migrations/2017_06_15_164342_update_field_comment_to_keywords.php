<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFieldCommentToKeywords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('keywords', function (Blueprint $table) {
            $table->dropColumn('common_id');
            $table->dropColumn('common_type');
            $table->integer('coupon_id')->comment('优惠券id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('keywords', function (Blueprint $table) {
            $table->dropColumn('coupon_id');
            $table->integer('common_id')->comment('关联表 ID');
            $table->string('common_type',30)->comment('关联表 类型');
        });
    }
}
