<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userIns = new \App\Models\User();
        $userIns->name = 'admin';
        $userIns->password = '123456';
        $userIns->type = 2;
        $userIns->save();

         factory(App\Models\User::class,101)->create();
    }
}
