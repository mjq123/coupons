<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time=\Carbon\Carbon::now();
        $menus = [
            ['id' => 1, 'name' => '菜单列表', 'parent_id' => 0, 'sort' => 100,'url' => '/manage/menu', 'icon' => 'window-restore', 'created_at' => $time],
            ['id' => 2, 'name' => '用户管理', 'parent_id' => 0, 'sort' => 50, 'url' => '/manage/users', 'icon' => 'user', 'created_at' => $time],
            ['id' => 3, 'name' => '评论管理', 'parent_id' => 0, 'sort' => 50, 'url' => '/manage/comment', 'icon' => 'comment', 'created_at' => $time],
            ['id' => 4, 'name' => 'Banner管理', 'parent_id' => 0, 'sort' => 50, 'url' => '/manage/banner', 'icon' => 'image', 'created_at' => $time],
            ['id' => 6, 'name' => '优惠券管理', 'parent_id' => 0, 'sort' => 50, 'url' => '/manage/coupon', 'icon' => 'gift', 'created_at' => $time],
            ['id' => 7, 'name' => '订单管理', 'parent_id' => 0, 'sort' => 50, 'url' => '/manage/order', 'icon' => 'leaf', 'created_at' => $time],

        ];
        DB::table('menus')->truncate();
        DB::table('menus')->insert($menus);
        //
    }
}
