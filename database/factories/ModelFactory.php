<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->userName,
        'openid' => str_random(28),
        'gender' => random_int(1, 3),
        'avatar' => $faker->imageUrl(),
        'city' => $faker->city
    ];
});
//优惠券
$factory->define(App\Models\Coupon::class, function (Faker\Generator $faker) {
    $userIds = \App\Models\User::pluck('id')->toArray();
    $a = [
        '优惠,券',
        '优惠，券',
        '大，小',
        '高,低',
    ];
    shuffle($a);
    return [
        'user_id' => $faker->randomElement($userIds),
        'name' => $faker->userName,
        'type' => rand(2,8),
        'limit' => $faker->text,
        'img' => $faker->imageUrl(),
        'price' => $faker->randomFloat(2,0,100),
        'prefer_type' => rand(1,2),
        'prefer_value' => $faker->randomFloat(2,0,100),
        'count' => rand(10,100),
        'use_count' => rand(0,5),
        'phone' => $faker->phoneNumber,
        'address' => $faker->address,
        'week' => rand(1,10),
        'time' => rand(1,4),
        'start' => $faker->date(),
        'end' => $faker->date(),
        'browse' => rand(0,1000),
        'status' => $faker->boolean,
        'verify' => rand(1,4),
        'longitude' => $faker->longitude,
        'latitude' => $faker->latitude,
        'content' => $a[0],
    ];
});
//订单
$factory->define(App\Models\Order::class, function (Faker\Generator $faker) {
    $userIds = \App\Models\User::pluck('id')->toArray();
    $couponIds = \App\Models\Coupon::pluck('id')->toArray();
    return [
        'user_id' => $faker->randomElement($userIds),
        'coupon_id' => $faker->randomElement($couponIds),
        'status' => rand(1,5),
        'money' => $faker->randomFloat(2,0,100),
        'order_no' => rand(100000000,999999999),
    ];
});
