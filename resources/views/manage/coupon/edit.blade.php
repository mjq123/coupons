@extends('manage.layouts.app')
@section('script')
    <script>
        $(function () {

            // 地图
            window.addEventListener('message', function (event) {
                var loc = event.data;
                if (loc && loc.module == 'locationPicker') {
                    $('input[name="longitude"]').val(loc.latlng.lng)
                    $('input[name="latitude"]').val(loc.latlng.lat)
                    $('input[name="address"]').val(loc.poiaddress)
                }
            }, false);
        })
    </script>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
            @include('manage.public.nav',['navs'=> [
                ['name' => '优惠券列表', 'url'=> '/manage/coupon'],
                ['name' => '编辑', 'active'=> 'true'],
            ]])


            <div class="panel panel-info">
                <div class="box-body">
                    {!! Form::model($info,['files' => true,'url' => '/manage/coupon/' . $info->id ,'method' => 'put', 'class'=> 'form-horizontal']) !!}

                    <div class="form-group">
                        {!! Form::label('name', '名称', ['class' => 'control-label col-sm-2']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('name', null, ['class' => 'form-control','required'=>'true']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('type', '类型', ['class' => 'control-label col-sm-2']) !!}
                        <div class="col-sm-10">
                            {!! Form::select('type', [
                                    '1' => '推荐',
                                    '2' => '美食',
                                    '3' => '娱乐',
                                    '4' => '酒店',
                                    '5' => '服务',
                                    '6' => '教育',
                                    '7' => '丽人',
                                    '8' => '其他',
                                ],null, ['class' => 'form-control','required'=>'true']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('prefer_type', '优惠类型', ['class' => 'control-label col-sm-2']) !!}
                        <div class="col-sm-10">
                            {!! Form::select('prefer_type',['1'=>'元','2'=>'折'] ,null, ['class' => 'form-control','required'=>'true']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('limit', '优惠范围', ['class' => 'control-label col-sm-2']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('limit',null, ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('img', '图片', ['class' => 'control-label col-sm-2']) !!}
                        <div class="col-sm-2">
                            {!! Form::file('img',['class' => 'form-control']) !!}
                        </div>
                        <div class="col-sm-3">
                            <img src="{{$info->img}}" alt="" style="height:80px;">
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('prefer_value', '优惠值', ['class' => 'control-label col-sm-2']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('prefer_value',null, ['class' => 'form-control','required'=>'true']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('count', '发布数量', ['class' => 'control-label col-sm-2']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('count',null, ['class' => 'form-control','required'=>'true']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('times', '最大领取数', ['class' => 'control-label col-sm-2']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('times',null, ['class' => 'form-control','required'=>'true']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('phone', '电话', ['class' => 'control-label col-sm-2']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('phone', null, ['class' => 'form-control','required' => 'true']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('price', '优惠券价格(元)', ['class' => 'control-label col-sm-2']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('price', null, ['class' => 'form-control','required' => 'true']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('content', '关键字', ['class' => 'control-label col-sm-2']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('content', null, ['class' => 'form-control','required' => 'true']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('start', '开始时间', ['class' => 'control-label col-sm-2']) !!}
                        <div class="col-sm-10">
                            {!! Form::date('start',null,['class' => 'form-control','required' => 'true']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('end', '结束时间', ['class' => 'control-label col-sm-2']) !!}
                        <div class="col-sm-10">
                            {!! Form::date('end',null,['class' => 'form-control','required' => 'true']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('week', '周几', ['class' => 'control-label col-sm-2']) !!}
                        <div class="col-sm-10">
                            {!! Form::select('week', [
                                    '1' => '周一',
                                    '2' => '周二',
                                    '3' => '周三',
                                    '4' => '周四',
                                    '5' => '周五',
                                    '6' => '周六',
                                    '7' => '周日',
                                    '8' => '工作日',
                                    '9' => '周末',
                                    '10' => '每天',
                                ],null, ['class' => 'form-control','required'=>'true']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('time', '时间段', ['class' => 'control-label col-sm-2']) !!}
                        <div class="col-sm-10">
                            {!! Form::select('time', [
                                '1' => '上午',
                                '2' => '下午',
                                '3' => '晚上',
                                '4' => '全天',
                            ],null, ['class' => 'form-control','required'=>'true']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('address', '地址', ['class' => 'control-label col-sm-2']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('address', null, ['class' => 'form-control', 'required']) !!}
                            <iframe id="mapPage" width="100%" height="600" frameborder=0
                                    src="http://apis.map.qq.com/tools/locpicker?search=1&type=1&key={{ env('TX_MAP_KEY') }}&referer=myapp">
                            </iframe>
                        </div>
                    </div>

                    <div class="col-sm-offset-2">
                        {!! Form::submit('保存', ['class' => 'btn btn-primary submit-button']) !!}
                    </div>
                    {!! Form::hidden('longitude', null, []) !!}
                    {!! Form::hidden('latitude', null, []) !!}
                    {!! Form::hidden('url', back()->getTargetUrl(), []) !!}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>


@endsection
