@extends('manage.layouts.app')
@section('head')
    <script charset="utf-8" src="http://map.qq.com/api/js?v=2.exp&key={{ env('TX_MAP_KEY') }}"></script>
@endsection
@section('script')
    <script>
        $(function () {
            // 地图
            var center = new qq.maps.LatLng({{ $info->latitude }}, {{ $info->longitude }});
            map = new qq.maps.Map( document.getElementById("map"), {
                    center: center,
                    zoom: 16,
                }
            );
            var marker = new qq.maps.Marker({
                position: center,
                map: map
            });

            // 重新审核
            $('.do-action .re-audit').click(function () {
                $('.audit').slideToggle()
                return false;
            })
            // 提交
            $('.audit a').click(function () {
                $('input[name="verify"]').val($(this).data('type'))
                $('.audit form').submit()
                return false;
            })
        })
    </script>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
            @include('manage.public.nav',['navs'=> [
                ['name' => '优惠券列表', 'url'=> '/manage/coupon'],
                ['name' => '优惠详情', 'active'=> 'true'],
            ]])

            <div class="panel panel-info">
                <div class="panel-heading">优惠券详情</div>
                <div class="panel-body">
                    <ul class="list-group">
                        <li>
                            <div class="col-sm-2 label"> 用户</div>
                            <div class="col-sm-10 text">
                                <img src="{{ $info->user->avatar }}" class="avatar">
                                {{ $info->user->name }}
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-2 label">名称</div>
                            <div class="col-sm-2 text"><img src="{{ $info->img }}" class="avatar">{{$info->name}}</div>
                            <div class="col-sm-2 label">分类:</div>
                            <div class="col-sm-2 text">
                                {{$info->typeText[$info->type]}}
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-2 label">优惠值:</div>
                            <div class="col-sm-2 text">
                                {{$info->prefer_value}}
                            </div>
                            <div class="col-sm-2 label">类型:</div>
                            <div class="col-sm-2 text">
                                {{$info->prefeText[$info->prefer_type]}}
                            </div>
                            <div class="col-sm-2 label">优惠券价格(元):</div>
                            <div class="col-sm-2 text">
                                {{$info->price}}
                            </div>

                        </li>
                        <li>
                            <div class="col-sm-2 label">总数量:</div>
                            <div class="col-sm-2 text">
                                {{$info->count}}
                            </div>
                            <div class="col-sm-2 label">已购买:</div>
                            <div class="col-sm-2 text">
                                {{$info->use_count}}
                            </div>
                            <div class="col-sm-2 label">剩余:</div>
                            <div class="col-sm-2 text">
                                {{$info->count-$info->use_count}}
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-2 label">生效时间:</div>
                            <div class="col-sm-2 text">
                                {{$info->start}}
                            </div>
                            <div class="col-sm-2 label">结束时间:</div>
                            <div class="col-sm-2 text">
                                {{$info->end}}
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-2 label">使用范围:</div>
                            <div class="col-sm-10 text">
                                {{$info->limit}}
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-2 label">关键字:</div>
                            <div class="col-sm-10 text">
                                @foreach(explode(',',$info->content) as $v)
                                    <a class="btn btn-success margin-r-5" data-type="2">{{$v}}</a>
                                @endforeach
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-2 label">地址:</div>
                            <div class="col-sm-10 text">
                                {{$info->address}}
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-2 label"></div>
                            <div class="col-sm-10 text">
                                <div id="map"></div>
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-2 label">电话</div>
                            <div class="col-sm-10 text"> {{ $info->phone }} </div>
                        </li>
                        <li>
                            <div class="col-sm-2 label">添加时间</div>
                            <div class="col-sm-2 text"> {{ $info->created_at }} </div>
                            <div class="col-sm-2 label">审核时间</div>
                            <div class="col-sm-2 text"> {{ $info->verified_at or '-'}} </div>
                        </li>
                        <li>
                            <div class="col-sm-2 label">当前状态</div>
                            <div class="col-sm-2 text">
                                    <span class="label label-{{ array_get([
                                    1=>'primary' ,
                                    2 => 'success',
                                    3 => 'danger',
                                    ],$info->verify) }}"> {{ $info->verifyText[$info->verify] }} </span>
                            </div>

                            <div class="col-sm-2 label">显示状态</div>
                            <div class="col-sm-2 text">
                                    <span class="label label-{{ array_get([
                                    1 => 'success',
                                    0 => 'danger'
                                    ],$info->status) }}"> {{ $info->statusText[$info->status] }} </span>
                            </div>
                        </li>

                        <li class="do-action">
                            <div class="col-sm-12 text-center">
                                <a class="btn btn-default" href="{{ url('/manage/coupon/'. $info->id.'/edit') }}">修改信息</a>
                                @if($info->verify != 1)
                                    <a href="" class="re-audit">重新审核</a>
                                @endif
                            </div>
                        </li>
                        <li class="audit" style="{{ $info->verify != 1 ? 'display: none' : '' }}">
                            <h3>审核：</h3>
                            <div class="col-sm-2 label">备注</div>
                            {!! Form::open(['url' => '/manage/coupon/audit/' . $info->id ,'method' => 'patch' ,'class'=> 'form-horizontal']) !!}
                            <div class="col-sm-10 text">
                                {!! Form::textarea('remark', null, ['class' => 'form-control', 'rows'=>5]) !!}
                            </div>
                            <div class="col-sm-10 text">
                                <a class="btn btn-success margin-r-5" data-type="2">通过</a>
                                <a class="btn btn-danger" data-type="3">驳回</a>
                                <a class="btn btn-danger" data-type="4">垃圾信息</a>
                                {!! Form::hidden('url', back()->getTargetUrl(), []) !!}
                                {!! Form::hidden('verify', 1, []) !!}
                            </div>
                            {!! Form::close() !!}
                        </li>
                    </ul>
                    </ul>
                </div>
            </div>

        </div>
    </div>


@endsection
