@extends('manage.layouts.app')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            @include('manage.public.nav',['navs'=> [
               ['name' => '优惠券', 'active'=> 'true'],
           ]])
            <div class="panel panel-info">
                <div class="panel-heading">筛选</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" id="search">
                        <div class="form-group">
                            <label class="col-md-1 control-label">名称</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="name"
                                       value="{{ \Illuminate\Support\Facades\Input::get('name') }}"/>
                            </div>
                            <label class="col-md-1 control-label">关键字</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="key"
                                       value="{{ \Illuminate\Support\Facades\Input::get('key') }}"/>
                            </div>
                            <label class="col-md-1 control-label">类别</label>
                            <div class="col-md-2">
                                <select  name="type" class="form-control">
                                    <option value="">不限</option>
                                    @foreach(app(\App\Models\Coupon::class)->typeText as $key => $item)
                                        <option value="{{ $key }}" {{ (\Illuminate\Support\Facades\Input::get('type') != '' && \Illuminate\Support\Facades\Input::get('type') == $key) ? 'selected': '' }}>{{ $item }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-offset-1 col-md-2">
                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i> 确定
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

            <div class="panel panel-info panel-table">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Id</th>
                            <th>用户</th>
                            <th>名称</th>
                            <th>图片</th>
                            <th>类别</th>
                            <th>优惠值(类型)</th>
                            <th>审核状态</th>
                            <th>商户地址</th>
                            <th>添加时间</th>
                            <th>操作</th>
                        </tr>

                        @foreach($lists as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td><a href="{{url('/manage/users/'.$item->user->id)}}">{{ str_limit($item->user->name,10) }}</a></td>
                                <td>{{ $item->name }}</td>
                                <td><img class="images" src="{{ $item->img }}" alt=""></td>
                                <td>{{ $item->typeText[$item->type] }}</td>
                                <td>{{ $item->prefer_value }} {{ $item->preferText[$item->prefer_type] }}</td>
                                <td> <span class="label label-{{ array_get([
                                    1=>'primary' ,
                                    2 => 'success',
                                    3 => 'warning',
                                    4 => 'danger'
                                    ],$item->verify) }}">{{ $item->verifyText[$item->verify] }}</span>
                                </td>
                                <td>{{ str_limit($item->address,10) }}</td>
                                <td>{{ $item->created_at }}</td>
                                <td>
                                    <a href="{{ url('/manage/coupon/hide/' . $item->id) }}" class="btn btn-default btn-sm patch">{{ $item->statusText[$item->status] }}</a>
                                    <a href="{{ url('/manage/coupon/' . $item->id) }}" class="btn btn-default btn-sm">详情</a>
                                    <a href="{{ url('/manage/coupon/' . $item->id.'/edit') }}" class="btn btn-default btn-sm">编辑</a>
                                    <a href="{{ url('/manage/coupon/' . $item->id) }}"
                                       class="btn btn-default btn-sm delete">删除</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    {{ $lists->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection()