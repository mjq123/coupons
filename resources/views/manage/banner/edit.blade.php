@extends('manage.layouts.app')
@section('content')
  <div class="row">
    <div class="col-xs-12">
      @include('manage.public.nav',['navs'=> [
          ['name' => 'Banner列表', 'url'=> '/manage/banner'],
          ['name' => 'Banner修改', 'active'=> 'true'],
      ]])
      <div class="panel panel-info">
        <div class="panel-heading">修改Banner</div>
        <div class="panel-body">
          {!! Form::model($info,['url' => '/manage/banner/' . $info->id ,'method' => 'put', 'class'=> 'form-horizontal','enctype'=>'multipart/form-data']) !!}
          <div class="col-sm-10 text">
          </div>
          <ul class="list-group">
            <li>
              <div class="col-sm-2 label">名称</div>
              <div class="col-sm-10 text">
                {!! Form::text('name', null, ['class' => 'form-control','required' => 'true']) !!}
              </div>
            </li>
            <li>
              <div class="col-sm-2 label">获取优惠券</div>
                <div class="col-md-3">
                    <select class="form-control" name="coupon_id">
                        @foreach($lists as $item)
                            <option value="{{ $item->id }}" class="form-control"  {{ ($info->coupon_id != '' &&  $info->coupon_id== $item->id) ? 'selected': '' }}>{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
            </li>
            <li>
              <div class="col-sm-2 label">图片</div>
              <div class="form-group">
                {!! Form::file('imgs',null,['required' => 'true'])!!}
              </div>
              <div class="form-group" style="margin-left:260px">
                <img src="{{$info->imgs}}" alt="" style="width:100px;height:100px">
              </div>

            </li>
            <li>
              <div class="btn-group pull-right">
                {!! Form::hidden('url', \Illuminate\Support\Facades\Input::get('url'), []) !!}
                {!! Form::submit('修改', ['class' => 'btn btn-primary submit-button']) !!}
              </div>
            </li>

          </ul>
          {!! Form::close() !!}
        </div>
      </div>
@endsection