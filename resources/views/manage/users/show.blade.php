@extends('manage.layouts.app')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            @include('manage.public.nav',['navs'=> [
                ['name' => '用户列表', 'url'=> '/manage/users'],
                ['name' => '用户详情', 'active'=> 'true'],
            ]])

            <div class="panel panel-info">
                <div class="panel-heading">用户详情</div>
                <div class="panel-body">
                    <ul class="list-group">

                        <li>
                            <div class="col-sm-2 label">用户</div>
                            <div class="col-sm-10 text">
                                <img src="{{ $info->avatar }}" class="avatar">
                                {{ $info->name }}
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-2 label">openid</div>
                            <div class="col-sm-10 text"> {{ $info->openid }} </div>
                        </li>
                        <li>
                            <div class="col-sm-2 label">性别</div>
                            <div class="col-sm-10 text"> {{ $info->genderText[$info->gender] }} </div>
                        </li>
                        <li>
                            <div class="col-sm-2 label">添加时间</div>
                            <div class="col-sm-2 text"> {{ $info->created_at }} </div>
                        </li>

                    </ul>
                </div>
            </div>
            @if(!empty($ins->toArray()) )
            <div class="panel panel-info">
                <div class="panel-heading">审核员</div>
                <div class="panel-body">
                    <ul class="list-group">
                        <li>
                            <div class="col-sm-2 label">用户</div>
                            <div class="col-sm-10 text">
                                <img src=" {{ $write_info['0']['user']['avatar']}}" class="avatar">
                                {{ $write_info['0']['user']['name']}}
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-2 label">添加时间</div>
                            <div class="col-sm-2 text"> {{  $write_info['0']['created_at'] }} </div>
                        </li>

                    </ul>
                </div>
            </div>
            @endif
            @if(!empty($order->toArray()) )
                <div class="panel panel-info">
                    <div class="panel-heading">Ta 的订单</div>
                    <div class="panel-body table-responsive">
                        <table class="table table-hover">
                            @foreach($order as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td class="images">
                                        <img src="{{ $item->coupon ? $item->coupon->img : '' }}">
                                    </td>
                                    <td>{{ $item->coupon ? $item->coupon->name : '' }}</td>
                                    <td>{{$item->money}}</td>
                                    <td>
                                    <span class="label label-{{ array_get([
                                    1=>'primary' ,
                                    2 => 'success',
                                    3 => 'warning',
                                    4 => 'danger',
                                    5 => 'danger',
                                    ],$item->status) }}"> {{ $item->statusText[$item->status] }} </span>
                                    </td>
                                    <td>{{ $item->created_at }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
               @endif
            @if(!empty($coupons->toArray()) )
                <div class="panel panel-info">
                    <div class="panel-heading">Ta 的评论</div>
                    <div class="panel-body table-responsive">
                        <table class="table table-hover">
                            @foreach($coupons as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td class="images">    
                                         {{ str_limit($item->content,10) }}
                                    </td>
                                    <td>
                                    <span class="label label-{{ array_get([
                                    1=>'primary' ,
                                    2 => 'success',
                                    3 => 'warning',
                                    ],$item->status) }}"> {{ $item->statusText[$item->status] }} </span>
                                    </td>
                          
                                    <td>{{ $item->created_at }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
               @endif

        </div>
    </div>
@endsection
