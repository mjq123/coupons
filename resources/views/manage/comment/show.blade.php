@extends('manage.layouts.app')
@section('script')
    <script>
        $(function () {
      
            // 重新审核
            $('.do-action .re-audit').click(function () {
                $('.audit').slideToggle()
                return false;
            })
            // 提交
            $('.audit a').click(function () {
                $('input[name="type"]').val($(this).data('type'))
                $('.audit form').submit()
                return false;
            })
        })
    </script>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12">
            @include('manage.public.nav',['navs'=> [
                ['name' => '评论列表', 'url'=> '/manage/comment'],
                ['name' => '评论详情', 'active'=> 'true'],
            ]])

            <div class="panel panel-info">
                <div class="panel-heading">用户详情</div>
                <div class="panel-body">
                    <ul class="list-group">
                        <li>
                            <div class="col-sm-2 label">用户</div>
                            <div class="col-sm-10 text">
                                <img src="{{ $info->user->avatar }}" class="avatar">
                                {{ $info->user->name }}
                            </div>
                        </li>

                        <li>
                            <div class="col-sm-2 label">评论内容</div>
                            <div class="col-sm-10 text"> {{ $info->content }} </div>
                        </li>

                        <li>
                            <div class="col-sm-2 label">添加时间</div>
                            <div class="col-sm-2 text"> {{ $info->created_at }} </div>
                        </li>
                        <li>
                            <div class="col-sm-2 label">状态</div>
                            <div class="col-sm-2 text">
                                <span class="label label-{{ array_get([
                                    1=>'primary' ,
                                    2 => 'success',
                                    3 => 'warning',
                                    ],$info->status) }}"> {{ $info->statusText[$info->status] }}
                                </span>
                            </div>
                        </li>
                        <li class="do-action">
                            <div class="col-sm-12 text-center">
                                @if($info->status != 1)
                                    <a href="" class="re-audit">重新审核</a>
                                @endif
                            </div>
                        </li>
                        <li class="audit" style="{{ $info->status != 1 ? 'display: none' : '' }}">
                            <h3>审核：</h3>
      
                             {!! Form::open(['url' => '/manage/comment/audit/' . $info->id ,'method' => 'patch' ,'class'=> 'form-horizontal']) !!}
                            <div class="col-sm-10 text">
                                {!! Form::hidden('url', back()->getTargetUrl(), []) !!}
                                {!! Form::hidden('type', 1, []) !!}
                            </div>
                            <div class="col-sm-10 col-sm-offset-2">
                                <a class="btn btn-success margin-r-5" data-type="1">通过</a>
                                <a class="btn btn-warning margin-r-5" data-type="2">驳回</a>
                                <a class="btn btn-danger" data-type="3">垃圾信息</a>
                            </div>
                            {!! Form::close() !!}
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>


@endsection
