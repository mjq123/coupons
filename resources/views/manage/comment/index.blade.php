@extends('manage.layouts.app')
@section('content')
     <div class="row">
        <div class="col-xs-12">
        	 @include('manage.public.nav',['navs'=> [
                ['name' => '评论列表', 'active'=> 'true'],
            ]])
            <div class="panel panel-info">
                <div class="panel-heading">筛选</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" id="search">
                        <div class="form-group">
                            <div class="row">
                                <label class="col-md-1 control-label">内容</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="content"
                                           value="{{ \Illuminate\Support\Facades\Input::get('content') }}"/>
                                </div>

                                <label class="col-md-1 control-label">状态</label>
                                <div class="col-md-3">
                                    <select class="form-control" name="status">
                                        <option value="">不限</option>
                                        @foreach(app(\App\Models\Comment::class)->statusText as $key => $item)
                                            <option value="{{ $key }}" {{ (\Illuminate\Support\Facades\Input::get('status') != '' && \Illuminate\Support\Facades\Input::get('status') == $key) ? 'selected': '' }}>{{ $item }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 20px">

                                <label class="col-md-1 control-label">用户名</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="name"
                                           value="{{ \Illuminate\Support\Facades\Input::get('name') }}"/>
                                </div>
                                <div class="col-md-offset-1 col-md-2">
                                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i> 确定
                                    </button>
                                </div>

                            </div>

                        </div>
                    </form>
                </div>
            </div>

            <div class="panel panel-info panel-table">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Id</th>
                            <th>用户</th>
                            <th>优惠券</th>
                            <th>内容</th>
                            <th>状态</th>
                            <th>创建时间</th>
                            <th>操作</th>
                        </tr>
                       @foreach($lists as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>
                                        @if($item->user)
                                         <a href="{{ url('/manage/users/' . $item->user->id) }}"
                                        class="btn btn-default btn-sm">{{$item->user->name}}</a>
                                        @else
                                        @endif
                                </td>
                                <td>
                                        @if($item->coupon)
                                        <a href="{{ url('/manage/coupon/' . $item->coupon->id) }}"
                                           class="btn btn-default btn-sm"> {{$item->coupon->name}}</a>
                                        @else
                                        @endif
                                </td>
                                <td>
                                    {{ str_limit($item->content,10) }}
                                </td>
                                <td>
                                    <span class="label label-{{ array_get([
                                    1=>'primary' ,
                                    2 => 'success',
                                    3 => 'warning',
                                    ],$item->status) }}"> {{ $item->statusText[$item->status] }}
                                    </span>
                                </td>
                                <td>{{ $item->created_at }}</td>
                                <td>
                                    <a href="{{ url('/manage/comment/show/' . $item->id) }}"
                                       class="btn btn-default btn-sm">详情</a>
                                    <a href="{{ url('/manage/comment/' . $item->id) }}"
                                       class="btn btn-default btn-sm delete">删除</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    {{ $lists->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection