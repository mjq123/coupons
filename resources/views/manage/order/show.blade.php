@extends('manage.layouts.app')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            @include('manage.public.nav',['navs'=> [
                ['name' => '订单列表', 'url'=> '/manage/order'],
                ['name' => '订单详情', 'active'=> 'true'],
            ]])

            <div class="panel panel-info">
                <div class="panel-heading">订单详情</div>
                <div class="panel-body">
                    <ul class="list-group">
                        <li>
                            <div class="col-sm-2 label"> 用户</div>
                            <div class="col-sm-10 text">
                                <img src="{{ $info->user->avatar }}" class="avatar">
                                {{ $info->user->name }}
                            </div>
                        </li>
                        <li>
                            <div class="col-sm-2 label">名称</div>
                            <div class="col-sm-10 text"><img src="{{ $info->coupon->img }}" class="avatar">{{$info->coupon->name}}</div>
                        </li>
                        <li>
                            <div class="col-sm-2 label">金额:</div>
                            <div class="col-sm-2 text">
                                {{$info->money}}
                            </div>
                            <div class="col-sm-2 label">创建时间:</div>
                            <div class="col-sm-2 text">
                                {{$info->created_at}}
                            </div>
                            <div class="col-sm-2 label">支付时间:</div>
                            <div class="col-sm-2 text">
                                {{$info->paid_at}}
                            </div>

                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>


@endsection
