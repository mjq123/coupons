@extends('manage.layouts.app')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            @include('manage.public.nav',['navs'=> [
               ['name' => '订单', 'active'=> 'true'],
           ]])
            <div class="panel panel-info">
                <div class="panel-heading">筛选</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" id="search">
                        <div class="form-group">
                            <label class="col-md-1 control-label">用户名</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="name"
                                       value="{{ \Illuminate\Support\Facades\Input::get('name') }}"/>
                            </div>
                            <label class="col-md-1 control-label">订单号</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="no"
                                       value="{{ \Illuminate\Support\Facades\Input::get('no') }}"/>
                            </div>
                            <label class="col-md-1 control-label">支付状态</label>
                            <div class="col-md-2">
                                <select  name="status" class="form-control">
                                    <option value="">不限</option>
                                    @foreach(app(App\Models\Order::class)->statusText as $key => $item)
                                        <option value="{{ $key }}" {{ (\Illuminate\Support\Facades\Input::get('status') != '' && \Illuminate\Support\Facades\Input::get('status') == $key) ? 'selected': '' }}>{{ $item }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-offset-1 col-md-2">
                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i> 确定
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

            <div class="panel panel-info panel-table">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Id</th>
                            <th>用户</th>
                            <th>订单号</th>
                            <th>优惠券</th>
                            <th>金额</th>
                            <th>支付状态</th>
                            <th>创建时间</th>
                            <th>操作</th>
                        </tr>

                        @foreach($lists as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td><a href="{{url('/manage/users/'.$item->user->id)}}">{{ str_limit($item->user->name,10) }}</a></td>
                                <td>{{ $item->order_no }}</td>
                                <td><a href="{{url('/manage/coupon/'.$item->coupon->id)}}">{{ str_limit($item->coupon->name,10) }}</a></td>
                                <td>{{ $item->money }}</td>
                                <td>{{ $item->statusText[$item->status] }}</td>
                                <td>{{ $item->created_at }}</td>
                                <td>
                                    <a href="{{ url('/manage/order/' . $item->id) }}" class="btn btn-default btn-sm">详情</a>
                                    <a href="{{ url('/manage/order/' . $item->id) }}"
                                       class="btn btn-default btn-sm delete">删除</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    {{ $lists->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection()